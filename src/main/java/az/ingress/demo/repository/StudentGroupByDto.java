package az.ingress.demo.repository;

public interface StudentGroupByDto {
    Integer getAge();
    Long getCount();
}

