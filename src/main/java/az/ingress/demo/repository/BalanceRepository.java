package az.ingress.demo.repository;

import az.ingress.demo.model.Balance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceRepository extends JpaRepository<Balance, Integer> {


}


