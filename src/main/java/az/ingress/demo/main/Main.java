package az.ingress.demo.main;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ReferenceQueue <Student> referenceQueue = new ReferenceQueue();

        PhantomReference<Student> phantomReference = new PhantomReference<>(new Student(),referenceQueue);
    }
}