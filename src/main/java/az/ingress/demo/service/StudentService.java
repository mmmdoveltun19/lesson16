package az.ingress.demo.service;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.model.Student;
import org.springframework.cache.annotation.CachePut;

public interface StudentService {
    StudentDto get(Integer id);

    StudentDto create(StudentDto student);

    StudentDto update(Integer id, Student student);

    @CachePut(key = "#id",cacheNames = "student")
    StudentDto update(Integer id, StudentDto student);

    void delete(Integer id);
}

